##Please before start deployment carefully read [Readme file](./README.md) file


DEPLOYMENT
=============================================================
In the Deployment folder there is crossoverWebApp.war it can 
be directly used for deployment, however there are environment
requirements.


ENVIRONMENT REQUIREMENTS
=============================================================
Default requirements that do not require rebuilding of webbapp

1. MySQL 5.7.9.0 server that can be accessible with using preferences:
Default database preferences:
> jdbc.driverClassName=com.mysql.jdbc.Driver
> jdbc.url=jdbc:mysql://localhost:3306/sales_db
> jdbc.username=root
> jdbc.password=secret  

if preferences of your database are different from preferences above crossoverWebApp.war
should be rebuilt before deploy (see BUILD in [Readme file](./README.md))
2. SQL scripts from SQL folder executed on MySQL server
3. Tomcat 8.0.28 version up and running with default preferences
and clean webapp/ROOT folder. and with no other webbapps.
4. JDK 1.8 installed and tomcat point to this JDK


DEPLOYMENT PROCESS
==============================================================

1. Stop tomcat server
2. Put crossoverWebApp.war (from the Deployment folder or new build) to the TOMCAT_HOME/webapps folder
3. Start tomcat server

APPLICATION ACCESS
===============================================================
After tomcat successfully started there are data access methods
available(copy from [Readme file](./README.md)):

1. GET localhost:8080/crossoverWebApp/order - get all orders
2. GET localhost:8080/crossoverWebApp/customer - get all customers
3. GET localhost:8080/crossoverWebApp/product - get all products
4. POST localhost:8080/crossoverWebApp/order - place new order
Request boby should contain JSON with order, for example:
`"{"orderId":null,"orderLines":[{"orderLineId":null,"product":{"productId":4,"description":"falcon","price":0.0,"quantity":0},"quantity":2}],"customer":{"customerId":2,"name":null,"creditLimit":0.0,"currentCredit":0.0,"address":null,"phone1":null,"phone2":null},"date":1445937307578,"totalPrice":0.0}"`
After POST request Server will:
    * Validate request
    * Check that is enough products in database to complete this order
    * Check that is enough credit limit left fro  customer
    * Place order and update customer, customer order, orderline, product tables with new calculated values.
5. GET localhost:8080/crossoverWebApp/product/{productId} - get specific product with id productId
6. DELETE localhost:8080/crossoverWebApp/order{orderId} - delete specific order with id order id. Also related orderLines will be deleted
7. PUT localhost:8080/crossoverWebApp/order/{id}  - Update given order with given orderId.
PUT request should also contain Request body, same template as for POST request.
Same business logic as for creating new order will be triggered.
