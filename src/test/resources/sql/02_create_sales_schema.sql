USE sales_db;

CREATE TABLE `product` (
  `productId`   INT(11)        NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(200)   NOT NULL,
  `price`       DECIMAL(15, 2) NOT NULL,
  `quantity`    INT(11)        NOT NULL,
  PRIMARY KEY (`productId`)
);

CREATE TABLE `customer` (
  `customerId`    INT(11)        NOT NULL AUTO_INCREMENT,
  `name`          VARCHAR(50)    NOT NULL,
  `creditLimit`   DECIMAL(15, 2) NOT NULL,
  `currentCredit` DECIMAL(15, 2) NOT NULL,
  `address`       VARCHAR(100)   NOT NULL,
  `phone1`        VARCHAR(100)   NOT NULL,
  `phone2`        VARCHAR(100)            DEFAULT NULL,
  PRIMARY KEY (`customerId`)
);

CREATE TABLE `customerOrder` (
  `orderId`    INT(11)  NOT NULL AUTO_INCREMENT,
  `customerId` INT(11)  NOT NULL,
  `date`       DATETIME NOT NULL,
  `totalPrice` DECIMAL(20, 2) NOT NULL,
  PRIMARY KEY (`orderId`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `customerorder_foreign_key` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`)
);

CREATE TABLE `orderline` (
  `orderLineId` INT(11) NOT NULL AUTO_INCREMENT,
  `productId`   INT(11) NOT NULL,
  `quantity`    INT(11) NOT NULL,
  `orderId`     INT(11) NOT NULL,
  PRIMARY KEY (`orderLineId`),
  KEY `productId` (`productId`),
  KEY `orderId` (orderId),
  CONSTRAINT `orderline_product_foreign_key` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`),
  CONSTRAINT `orderline_order_foreign_key` FOREIGN KEY (`orderId`) REFERENCES `customerorder` (`orderId`)
);



