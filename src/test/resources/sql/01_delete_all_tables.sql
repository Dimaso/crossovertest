SET FOREIGN_KEY_CHECKS = 0;
drop table if exists customer;
drop table if exists product;
drop table if exists orderline;
drop table if exists customerorder;
SET FOREIGN_KEY_CHECKS = 1;