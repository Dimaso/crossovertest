INSERT INTO sales_db.product (description, price, quantity) VALUES ("x-wing", 100.00, 20);
INSERT INTO sales_db.product (description, price, quantity) VALUES ("tie-fighter", 150.00, 20);
INSERT INTO sales_db.product (description, price, quantity) VALUES ("at-et", 500.00, 25);
INSERT INTO sales_db.product (description, price, quantity) VALUES ("falcon", 10.00, 200);

INSERT INTO sales_db.customer (name, creditLimit, currentCredit, address, phone1, phone2)
VALUES("Dima", 5000.00, 0.00, "Menno", "2560213", "32142");
INSERT INTO sales_db.customer (name, creditLimit, currentCredit, address, phone1, phone2)
VALUES("Dima", 5000.00, 0.00, "Menno", "2560213", "32142");
INSERT INTO sales_db.customer (name, creditLimit, currentCredit, address, phone1, phone2)
VALUES("Alex", 5000.00, 100.00, "Menno van", "2560213", "32142");

INSERT INTO sales_db.customerorder (customerId, date, totalPrice) VALUES (1, NOW(), 100.00);
INSERT INTO sales_db.customerorder (customerId, date, totalPrice) VALUES (2, NOW(), 100.00);
INSERT INTO sales_db.customerorder (customerId, date, totalPrice) VALUES (3, NOW(), 100.00);

INSERT INTO sales_db.orderline (productId, quantity, orderId) VALUES (1, 3, 1);
INSERT INTO sales_db.orderline (productId, quantity, orderId) VALUES (3, 3, 1);
INSERT INTO sales_db.orderline (productId, quantity, orderId) VALUES (4, 1, 2);