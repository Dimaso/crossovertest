package controller;

import com.crossover.domain.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Iterator;
import java.util.List;

import static junit.framework.TestCase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by Dmitriy on 10.11.2015.
 * test class for /customer Controller
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
@WebAppConfiguration
public class MvcCustomerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper mapper;

    private final static String CUSTOMER_URI = "/customer";

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.mapper = new ObjectMapper();
    }

    @Test
    public void getCustomersTest() throws Exception {
        MvcResult result = mockMvc.perform(get(CUSTOMER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Customer> customers = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Customer.class));
        Iterator<Customer> customerIterator = customers.iterator();
        Customer customer = customerIterator.next();
        assertEquals("Dima", customer.getName());
        assertEquals("Menno", customer.getAddress());
        assertEquals(0, customer.getCurrentCredit(), 0.1);
        assertEquals(5000, customer.getCreditLimit(), 0.1);
    }

    @Test
    public void getCustomerTest() throws Exception {
        String customersContent = mockMvc.perform(get(CUSTOMER_URI))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<Customer> customers = mapper
                .readValue(customersContent, mapper.getTypeFactory().constructCollectionType(List.class, Customer.class));
        assertFalse(customers.isEmpty());
        int customerId = customers.get(0).getCustomerId();
        String customerContent = mockMvc.perform(get(CUSTOMER_URI + "/" + customerId))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Customer customer = mapper.readValue(customerContent, Customer.class);
        assertEquals(customerId, customer.getCustomerId().intValue());
        assertNotNull(customer.getName());
    }

    //TODO All customer tests
}
