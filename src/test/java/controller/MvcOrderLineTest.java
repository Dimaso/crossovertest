package controller;

import com.crossover.domain.OrderLine;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Iterator;
import java.util.List;

import static junit.framework.TestCase.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by Dmitriy on 10.11.2015.
 * Test class for /orderLine Controller
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@WebAppConfiguration
public class MvcOrderLineTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper mapper;

    private final static String ORDER_LINE_URI = "/orderLine";

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.mapper = new ObjectMapper();
    }

    @Test
    public void getCustomersTest() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_LINE_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<OrderLine> orderLines = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, OrderLine.class));
        Iterator<OrderLine> customerIterator = orderLines.iterator();
        OrderLine orderLine = customerIterator.next();
        assertEquals(orderLine.getQuantity(), 3);
    }

    @Test
    public void getCustomerTest() throws Exception {
        String customersContent = mockMvc.perform(get(ORDER_LINE_URI))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<OrderLine> orderLines = mapper
                .readValue(customersContent, mapper.getTypeFactory().constructCollectionType(List.class, OrderLine.class));
        assertFalse(orderLines.isEmpty());
        int customerId = orderLines.get(0).getOrderLineId();
        String customerContent = mockMvc.perform(get(ORDER_LINE_URI + "/" + customerId))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        OrderLine orderLine = mapper.readValue(customerContent, OrderLine.class);
        assertEquals(customerId, orderLine.getOrderLineId().intValue());
        assertNotNull(orderLine.getQuantity());
    }


}
