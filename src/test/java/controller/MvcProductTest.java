package controller;

import com.crossover.domain.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Iterator;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by Dmitriy on 10.11.2015.
 * Test class for "/product" controller
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
@WebAppConfiguration
public class MvcProductTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper mapper;

    private static final String PRODUCT_URI = "/product";

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.mapper = new ObjectMapper();
    }

    @Test
    public void getProductsTest() throws Exception {
        MvcResult result = mockMvc.perform(get(PRODUCT_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Product> products = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Product.class));
        Iterator<Product> productIterator = products.iterator();
        Product product = productIterator.next();
        assertEquals("x-wing", product.getDescription());
        assertEquals(100.00, product.getPrice(), 0.1);
        assertEquals(20, product.getQuantity());
    }

    @Test
    public void getProductTest() throws Exception {
        String productsContent = mockMvc.perform(get(PRODUCT_URI))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<Product> products = mapper
                .readValue(productsContent, mapper.getTypeFactory().constructCollectionType(List.class, Product.class));
        assertFalse(products.isEmpty());
        int productId = products.get(0).getProductId();
        String productContent = mockMvc.perform(get(PRODUCT_URI + "/" + productId))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Product product = mapper.readValue(productContent, Product.class);
        assertEquals(productId, product.getProductId().intValue());
        assertNotNull(product.getDescription());
    }

    // TODO all product tests
}
