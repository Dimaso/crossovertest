package controller;

import com.crossover.domain.Customer;
import com.crossover.domain.Order;
import com.crossover.domain.OrderLine;
import com.crossover.domain.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by Dmitriy on 10.11.2015.
 * Test for /order Controller
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
@WebAppConfiguration
public class MvcOrderTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper mapper;

    private final static String ORDER_URI = "/order";


    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.mapper = new ObjectMapper();
    }

    @Test
    public void getOrdersTest() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Order> orders = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        Iterator<Order> orderIterator = orders.iterator();
        Order order = orderIterator.next();
        List<OrderLine> orderLines = order.getOrderLines();
        OrderLine orderLine = orderLines.get(0);
        assertEquals(3, orderLine.getQuantity());
        Product product = new Product(1, "x-wing", 100.00, 20);
        assertEquals(product, orderLine.getProduct());
    }

    @Test
    public void getOrderTest() throws Exception {
        String ordersContent = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<Order> orders = mapper
                .readValue(ordersContent, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        assertFalse(orders.isEmpty());
        int orderId = orders.get(0).getOrderId();
        String orderContent = mockMvc.perform((get(ORDER_URI + "/" + orderId)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Order order = mapper.readValue(orderContent, Order.class);
        assertEquals(orderId, order.getOrderId().intValue());
        assertNotNull(order.getDate());
    }

    @Test
    public void testPlaceOrder() throws Exception {
        Product product = new Product(4, "falcon", 0, 0);
        OrderLine orderLine = new OrderLine(product, 2);
        List<OrderLine> orderLines = new ArrayList<>();
        orderLines.add(orderLine);
        Customer customer = new Customer(2);
        Order order = new Order(orderLines, customer, new Date());
        mockMvc.perform(post(ORDER_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(order)))
                .andExpect(status().isOk());
    }

    @Test
    public void testPlaceOrderNoProductException() throws Exception {
        Product product = new Product(21123123, "Dummy product", 0, 0);
        OrderLine orderLine = new OrderLine(product, 2);
        List<OrderLine> orderLines = new ArrayList<>();
        orderLines.add(orderLine);
        Customer customer = new Customer(2);
        Order order = new Order(orderLines, customer, new Date());
        mockMvc.perform(post(ORDER_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(order)))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void testPlaceOrderNoCustomerException() throws Exception {
        Product product = new Product(2, "falcon", 0, 0);
        OrderLine orderLine = new OrderLine(product, 2);
        List<OrderLine> orderLines = new ArrayList<>();
        orderLines.add(orderLine);
        Customer customer = new Customer(999999999);
        Order order = new Order(orderLines, customer, new Date());
        mockMvc.perform(post(ORDER_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(order)))
                .andExpect(status().is5xxServerError());
    }


    @Test
    public void testDeleteOrder() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Order> orders = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        if (orders != null && !orders.isEmpty()) {
            mockMvc.perform(delete("/order/" + orders.get(orders.size() - 1).getOrderId()))
                    .andExpect(status().isOk());
        }
    }

    @Test
    public void testDeleteOrderNoOrder() throws Exception {
        mockMvc.perform(delete("/order/" + 99999999))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void testUpdateOrder() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Order> orders = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        if (orders != null && !orders.isEmpty()) {
            Order lastOrder = orders.get(orders.size() - 1);
            Product product = new Product(4, "falcon", 0, 0);
            OrderLine orderLine = new OrderLine(product, 2);
            List<OrderLine> orderLines = new ArrayList<>();
            orderLines.add(orderLine);
            Customer customer = new Customer(2);
            Order order = new Order(lastOrder.getOrderId(), orderLines, customer, new Date());
            mockMvc.perform(put("/order/" + order.getOrderId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(order)))
                    .andExpect(status().isOk());
        }
    }

    @Test
    public void testUpdateOrderNoProductException() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Order> orders = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        if (orders != null && !orders.isEmpty()) {
            Order lastOrder = orders.get(orders.size() - 1);
            Product product = new Product(499999999, "Dummy product", 0, 0);
            OrderLine orderLine = new OrderLine(product, 2);
            List<OrderLine> orderLines = new ArrayList<>();
            orderLines.add(orderLine);
            Customer customer = new Customer(2);
            Order order = new Order(lastOrder.getOrderId(), orderLines, customer, new Date());
            mockMvc.perform(put("/order/" + order.getOrderId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(order)))
                    .andExpect(status().is5xxServerError());
        }
    }

    @Test
    public void testUpdateOrderNoCustomerException() throws Exception {
        MvcResult result = mockMvc.perform(get(ORDER_URI))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        List<Order> orders = mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, Order.class));
        if (orders != null && !orders.isEmpty()) {
            Order lastOrder = orders.get(orders.size() - 1);
            Product product = new Product(4, "falcon", 0, 0);
            OrderLine orderLine = new OrderLine(product, 2);
            List<OrderLine> orderLines = new ArrayList<>();
            orderLines.add(orderLine);
            Customer customer = new Customer(999999999);
            Order order = new Order(lastOrder.getOrderId(), orderLines, customer, new Date());
            mockMvc.perform(put("/order/" + order.getOrderId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(order)))
                    .andExpect(status().is5xxServerError());
        }
    }
}
