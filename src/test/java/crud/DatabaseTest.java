package crud;

import com.crossover.domain.Customer;
import com.crossover.domain.Order;
import com.crossover.domain.OrderLine;
import com.crossover.domain.Product;
import com.crossover.logic.ApplicationService;
import com.crossover.logic.Notifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Dmitriy on 25.10.2015.
 * Check basic CRUD operations directly
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
public class DatabaseTest {

    @Autowired
    @Qualifier("applicationServiceImpl")
    ApplicationService applicationService;

    @Test
    public void fillOrderNoProduct() throws Exception {
        Product product = new Product(100000000, "x-wing", 100.00, 2);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 3));

        Customer customer = new Customer("Test", 1000, 0);
        Order order = new Order(orderLineList, customer, new Date());

        Notifier notifier = applicationService.createOrder(order);
        assertTrue(notifier.hasErrors());
    }

    @Test
    public void fillOrderNoCustomer() throws Exception {
        Product product = new Product(1, "x-wing", 100.00, 2);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 3));

        Customer customer = new Customer(10000000);
        Order order = new Order(orderLineList, customer, new Date());

        Notifier notifier = applicationService.createOrder(order);
        assertTrue(notifier.hasErrors());
    }

    @Test
    public void testCrud(){
        Customer customer = applicationService.getCustomerById(1);

        assertEquals("Dima", customer.getName());
        assertEquals("Menno", customer.getAddress());
        assertEquals(5000.00, customer.getCreditLimit(), 0.1);
        assertEquals(0.00, customer.getCurrentCredit(), 0.1);
        assertEquals("2560213", customer.getPhone1());
        assertEquals("32142", customer.getPhone2());

        List<Order> orders = customer.getOrders();

        Order order = orders.get(0);
        assertEquals(customer, order.getCustomer());
        assertEquals(1, order.getOrderId().intValue());

        List<OrderLine> orderLines = order.getOrderLines();

        Iterator<OrderLine> orderLineIterator = orderLines.iterator();
        OrderLine orderLine = orderLineIterator.next();
        Product product = orderLine.getProduct();
        assertEquals("x-wing", product.getDescription());
        assertEquals(3, orderLine.getQuantity());

        OrderLine orderLine1 = orderLineIterator.next();
        Product product1 = orderLine1.getProduct();
        assertEquals("at-et", product1.getDescription());
        assertEquals(3, orderLine1.getQuantity());
    }

}
