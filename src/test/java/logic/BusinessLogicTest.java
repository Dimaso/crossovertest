package logic;

import com.crossover.domain.Customer;
import com.crossover.domain.Order;
import com.crossover.domain.OrderLine;
import com.crossover.domain.Product;
import com.crossover.logic.ApplicationService;
import com.crossover.logic.Notifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static logic.Helper.calculateTotalPrice;

/**
 * Created by Dmitriy on 10.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
public class BusinessLogicTest {


    @Autowired
    @Qualifier("applicationServiceImpl")
    private ApplicationService applicationService;

    @Test
    public void testCanPlaceOrder() {
        Product product = new Product(1, "x-wing", 100.00, 10);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 1));

        Customer customer = new Customer("Test", 1000, 100);
        Order order = new Order(orderLineList, customer, new Date());
        order.setTotalPrice(calculateTotalPrice(orderLineList));

        assertTrue("Order should be placed", applicationService.canPlaceOrder(order));
    }

    @Test
    public void testCanNotPlaceOrderCredit() {
        Product product = new Product(1, "x-wing", 100.00, 10);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 1));

        Customer customer = new Customer("Test", 1000, 1000);
        Order order = new Order(orderLineList, customer, new Date());
        order.setTotalPrice(calculateTotalPrice(orderLineList));

        assertFalse("CurrentCredit > CreditLimit", applicationService.canPlaceOrder(order));
    }

    @Test
    public void testCanNotCreateOrder() {
        Product product = new Product(1, "x-wing", 100.00, 10);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 1000000));

        Customer customer = new Customer(1);
        Order order = new Order(orderLineList, customer, new Date());
        order.setTotalPrice(calculateTotalPrice(orderLineList));

        Notifier notifier = applicationService.createOrder(order);

        assertTrue(notifier.hasErrors());
        assertEquals("Check credit limit and product amount", notifier.getErrors().get(0).get("orderNotPlaced"));

    }

    @Test
    public void testCanNotPlaceOrderProducts() {
        Product product = new Product(1, "x-wing", 100.00, 2);

        List<OrderLine> orderLineList = Arrays.asList(new OrderLine(product, 3));

        Customer customer = new Customer("Test", 1000, 0);
        Order order = new Order(orderLineList, customer, new Date());
        order.setTotalPrice(calculateTotalPrice(orderLineList));

        assertFalse("Amount of products in order > quantity", applicationService.canPlaceOrder(order));
    }


}
