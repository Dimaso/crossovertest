package logic;

import com.crossover.logic.impl.Notification;
import com.crossover.logic.Notifier;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotificationTest {

    @Test
    public void testNoErrors() {
        Notifier notifier = new Notification();
        assertFalse(notifier.hasErrors());
    }

    @Test
    public void testHasErrors() {
        Notifier notifier = new Notification();
        String errorReason = "testError";
        String errorMessage = "error for test";
        notifier.addError(errorReason, errorMessage);

        assertTrue(notifier.hasErrors());
        assertEquals(errorMessage, notifier.getErrors().get(0).get(errorReason));
    }

    @Test
    public void testGetErrors() {
        Notifier notifier = new Notification();
        notifier.addError("testError", "error for test");
        notifier.addError("testError1", "error for test");
        notifier.addError("testError2", "error for test");

        assertEquals(3, notifier.getErrors().size());
    }



}