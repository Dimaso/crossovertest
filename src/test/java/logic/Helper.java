package logic;

import com.crossover.domain.OrderLine;

import java.util.List;

/**
 * Created by Dmitriy on 10.11.2015.
 */
public class Helper {

    public static double calculateTotalPrice(List<OrderLine> orderLineList) {
        return orderLineList.stream().mapToDouble(o -> o.getQuantity() * o.getProduct().getPrice()).sum();
    }
}
