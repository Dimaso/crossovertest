package com.crossover.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dmitriy on 25.10.2015.
 */
@Entity
@Table(name = "orderline")
public class OrderLine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer orderLineId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "orderId", nullable = false, insertable = false, updatable = false)
    private Order order;

    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    @Column
    private int quantity;

    public OrderLine() {
    }

    public OrderLine(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Integer getOrderLineId() {
        return orderLineId;
    }

    public void setOrderLineId(Integer orderLineId) {
        this.orderLineId = orderLineId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderLine)) return false;

        OrderLine orderLine = (OrderLine) o;

        if (quantity != orderLine.quantity) return false;
        if (order != null ? !order.equals(orderLine.order) : orderLine.order != null) return false;
        if (orderLineId != null ? !orderLineId.equals(orderLine.orderLineId) : orderLine.orderLineId != null)
            return false;
        return !(product != null ? !product.equals(orderLine.product) : orderLine.product != null);

    }

    @Override
    public int hashCode() {
        int result = orderLineId != null ? orderLineId.hashCode() : 0;
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + quantity;
        return result;
    }
}
