package com.crossover.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dmitriy on 25.10.2015.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer productId;

    @Column(length = 200)
    private String description;

    @Column(length = 15, precision = 2)
    private double price;

    @Column
    private int quantity;

    public Product() {
    }

    public Product(int id, String description, double price, int quantity) {
        this.productId = id;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void decreaseQuantity(int orderQuantity) {
        quantity -= orderQuantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;

        if (Double.compare(product.price, price) != 0) return false;
        if (quantity != product.quantity) return false;
        if (description != null ? !description.equals(product.description) : product.description != null) return false;
        return !(productId != null ? !productId.equals(product.productId) : product.productId != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + quantity;
        return result;
    }
}
