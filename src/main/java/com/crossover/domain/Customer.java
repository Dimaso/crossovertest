package com.crossover.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 25.10.2015.
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer customerId;

    @Column(length = 50)
    private String name;

    @Column(length = 15, precision = 2)
    private double creditLimit;

    @Column(length = 15, precision = 2)
    private double currentCredit;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "customerId")
    private List<Order> orders;

    @Column(length = 100)
    private String address;

    @Column(length = 100)
    private String phone1;

    @Column(length = 100)
    private String phone2;

    public Customer() {
    }

    public Customer(int id, String name, double creditLimit, double currentCredit,
                    List<Order> orders, String address, String phone1, String phone2) {
        this.customerId = id;
        this.name = name;
        this.creditLimit = creditLimit;
        this.currentCredit = currentCredit;
        this.orders = orders;
        this.address = address;
        this.phone1 = phone1;
        this.phone2 = phone2;
    }

    public Customer(String name, double creditLimit, double currentCredit) {
        this.name = name;
        this.creditLimit = creditLimit;
        this.currentCredit = currentCredit;
    }

    public Customer(int customerId) {
        this.customerId = customerId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public double getCurrentCredit() {
        return currentCredit;
    }

    public void setCurrentCredit(double currentCredit) {
        this.currentCredit = currentCredit;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;

        Customer customer = (Customer) o;

        if (Double.compare(customer.creditLimit, creditLimit) != 0) return false;
        if (Double.compare(customer.currentCredit, currentCredit) != 0) return false;
        if (address != null ? !address.equals(customer.address) : customer.address != null) return false;
        if (customerId != null ? !customerId.equals(customer.customerId) : customer.customerId != null) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        if (orders != null ? !orders.equals(customer.orders) : customer.orders != null) return false;
        if (phone1 != null ? !phone1.equals(customer.phone1) : customer.phone1 != null) return false;
        return !(phone2 != null ? !phone2.equals(customer.phone2) : customer.phone2 != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = customerId != null ? customerId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(creditLimit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(currentCredit);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (orders != null ? orders.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phone1 != null ? phone1.hashCode() : 0);
        result = 31 * result + (phone2 != null ? phone2.hashCode() : 0);
        return result;
    }
}
