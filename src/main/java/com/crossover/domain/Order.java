package com.crossover.domain;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dmitriy on 25.10.2015.
 */
@Entity
@Table(name = "customerorder")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer orderId;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn(name = "orderId", nullable = false, insertable = false, updatable = false)
    private List<OrderLine> orderLines;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customerId")
    private Customer customer;

    @Column
    @JsonSerialize(using = DateSerializer.class)
    private Date date;

    @Column
    private double totalPrice;

    public Order() {
    }

    public Order(List<OrderLine> orderLines, Customer customer, Date date) {
        this.orderLines = orderLines;
        this.customer = customer;
        this.date = date;
    }

    public Order(int orderId, List<OrderLine> orderLines, Customer customer, Date date) {
        this.orderId = orderId;
        this.orderLines = orderLines;
        this.customer = customer;
        this.date = date;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (customer != null ? !customer.equals(order.customer) : order.customer != null) return false;
        if (date != null ? !date.equals(order.date) : order.date != null) return false;
        if (orderId != null ? !orderId.equals(order.orderId) : order.orderId != null) return false;
        return !(orderLines != null ? !orderLines.equals(order.orderLines) : order.orderLines != null);

    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + (orderLines != null ? orderLines.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
