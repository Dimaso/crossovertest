package com.crossover.logic.impl;

import com.crossover.logic.Notifier;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 14.11.2015.
 * Implementation of notifier pattern. Needed to get rid of throwing exceptions to controller
 */
public class Notification implements Notifier {

    List<JSONObject> errorList = new ArrayList<>();

    @Override
    public boolean hasErrors() {
        return !errorList.isEmpty();
    }

    @Override
    public void addError(String reason, String errorMessage) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(reason, errorMessage);
        errorList.add(jsonObject);
    }

    @Override
    public List<JSONObject> getErrors() {
        return errorList;
    }
}
