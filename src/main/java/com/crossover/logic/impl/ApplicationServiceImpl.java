package com.crossover.logic.impl;

import com.crossover.domain.Customer;
import com.crossover.domain.Order;
import com.crossover.domain.OrderLine;
import com.crossover.domain.Product;
import com.crossover.logic.ApplicationService;
import com.crossover.logic.Notifier;
import com.crossover.repository.CustomerRepository;
import com.crossover.repository.OrderLineRepository;
import com.crossover.repository.OrderRepository;
import com.crossover.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Dmitriy on 25.10.2015.
 * Server Business logic
 */
@Service
@Repository
public class ApplicationServiceImpl implements ApplicationService {

    private final static Logger logger = LoggerFactory.getLogger(ApplicationServiceImpl.class);

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OrderLineRepository orderLineRepository;

    @Override
    public Iterable<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<OrderLine> getAllOrderLines() {
        return orderLineRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Order getOrderById(int id) {
        return orderRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Customer getCustomerById(int id) {
        return customerRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Product getProductById(int id) {
        return productRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderLine getOrderLineById(int id) {
        return orderLineRepository.findOne(id);
    }

    @Override
    public Notifier createOrder(Order order) {
        Notifier notifier = fillOrder(order);
        if (notifier.hasErrors()) {
            return notifier;
        } else if (canPlaceOrder(order)) {
            placeOrder(order);
        } else {
            notifier.addError("orderNotPlaced", "Check credit limit and product amount");
        }
        return notifier;
    }

    @Override
    @Transactional(readOnly = true)
    public Notifier fillOrder(Order order) {
        Notifier notifier = new Notification();
        for (OrderLine orderLine : order.getOrderLines()) {
            Product repositoryProduct = productRepository.findOne(orderLine.getProduct().getProductId());
            if (repositoryProduct != null) {
                orderLine.setProduct(repositoryProduct);
            } else {
                logger.debug("No product available, product id: " + orderLine.getProduct().getProductId());
                notifier.addError("productId_" +  orderLine.getProduct().getProductId() + "notFound",
                        "Can not find product in database");
                return notifier;
            }
            Customer customerFromDb = customerRepository.findOne(order.getCustomer().getCustomerId());
            if (customerFromDb != null) {
                order.setCustomer(customerFromDb);
            } else {
                logger.debug("No such customer id: " + order.getCustomer().getCustomerId());
                notifier.addError("customerID_" +  order.getCustomer().getCustomerId() + "notFound",
                        "Can not find customer in database");
                return notifier;
            }
        }
        double orderPrice =
                order.getOrderLines().stream().mapToDouble(o -> o.getQuantity() * o.getProduct().getPrice()).sum();
        order.setTotalPrice(orderPrice);
        return notifier;
    }

    @Override
    public boolean canPlaceOrder(Order order) {
        return isCustomerLimitEnough(order)
                && isProductAmountEnough(order.getOrderLines());
    }

    private boolean isCustomerLimitEnough(Order order) {
        Customer customer = order.getCustomer();
        logger.debug("Total order price: " + order.getTotalPrice() + "customer credit limit "
                + customer.getCreditLimit() + "customer current credit" + customer.getCurrentCredit());
        return customer.getCreditLimit() >= customer.getCurrentCredit() + order.getTotalPrice();
    }

    private boolean isProductAmountEnough(List<OrderLine> orderLines) {
        for (OrderLine orderLine : orderLines) {
            if (orderLine.getProduct().getQuantity() < orderLine.getQuantity()) {
                logger.debug("Not enough quantity for product " + orderLine.getProduct().getQuantity());
                return false;
            }
        }
        return true;
    }

    @Override
    @Transactional
    public void placeOrder(Order order) {
        for (OrderLine orderLine : order.getOrderLines()) {
            orderLine.getProduct().decreaseQuantity(orderLine.getQuantity());
            productRepository.save(orderLine.getProduct());
            orderLine.setOrder(order);
        }
        updateCustomerLimit(order.getCustomer(), order.getTotalPrice());
        orderRepository.save(order);
    }

    private void updateCustomerLimit(Customer customer, double totalPrice) {
        customer.setCurrentCredit(customer.getCurrentCredit() + totalPrice);
        customerRepository.save(customer);
    }

    @Override
    @Transactional
    public Notifier deleteOrder(int id) {
        Notifier notifier = new Notification();
        Order order = orderRepository.findOne(id);
        if (order != null) {
            orderRepository.delete(order);
        } else {
            notifier.addError("orderNotExists_" + id, "Can not find order to delete");
            logger.warn("Can not find order to delete, orderId: " + id);
        }
        return notifier;
    }

}
