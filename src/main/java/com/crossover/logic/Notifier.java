package com.crossover.logic;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Dmitriy on 14.11.2015.
 * Interface for notifier pattern
 */
public interface Notifier {

    boolean hasErrors();

    void addError(String reason, String errorMessage);

    List<JSONObject> getErrors();

}
