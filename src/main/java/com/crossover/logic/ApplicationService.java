package com.crossover.logic;

import com.crossover.domain.Customer;
import com.crossover.domain.Order;
import com.crossover.domain.OrderLine;
import com.crossover.domain.Product;

/**
 * Created by Dmitriy on 25.10.2015.
 * Main application service responsible for business logic
 */
public interface ApplicationService {

    Iterable<Order> getAllOrders();

    Iterable<Customer> getAllCustomers();

    Iterable<Product> getAllProducts();

    Iterable<OrderLine> getAllOrderLines();

    Order getOrderById(int id);

    Customer getCustomerById(int id);

    Product getProductById(int id);

    OrderLine getOrderLineById(int id);

    boolean canPlaceOrder(Order order);

    Notifier createOrder(Order order);

    void placeOrder(Order order);

    Notifier fillOrder(Order order);

    Notifier deleteOrder(int id);
}
