package com.crossover.controller;

import com.crossover.domain.Order;
import com.crossover.logic.ApplicationService;
import com.crossover.logic.Notifier;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 08.11.2015.
 * Controller for orders
 */

@Controller
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    @Qualifier("applicationServiceImpl")
    private ApplicationService applicationService;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Order>> getOrders() {
        Iterable<Order> allOrders = applicationService.getAllOrders();
        return new ResponseEntity<>(allOrders, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Order> getOrder(@PathVariable int id) {
        Order order = applicationService.getOrderById(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<List<JSONObject>> placeOrder(@RequestBody Order order) {
        Notifier notifier = applicationService.createOrder(order);
        if (notifier.hasErrors()) {
            return new ResponseEntity<>(notifier.getErrors(), HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<List<JSONObject>> deleteOrder(@PathVariable int id) {
        Notifier notifier = applicationService.deleteOrder(id);
        if (notifier.hasErrors()) {
            return new ResponseEntity<>(notifier.getErrors(), HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<List<JSONObject>> updateOrder(@RequestBody Order order, @PathVariable int id) {
            order.setOrderId(id);
        Notifier notifier = applicationService.createOrder(order);
        if (notifier.hasErrors()) {
            return new ResponseEntity<>(notifier.getErrors(), HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        }
    }
}
