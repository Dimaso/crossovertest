package com.crossover.controller;

import com.crossover.domain.Customer;
import com.crossover.logic.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Dmitriy on 08.11.2015.
 * Controller for customers
 */
@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    @Qualifier("applicationServiceImpl")
    private ApplicationService applicationService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Customer>> getCustomers() {
        Iterable<Customer> allCustomers = applicationService.getAllCustomers();
        logger.debug("Get customers executed");
        return new ResponseEntity<>(allCustomers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getCustomer(@PathVariable int id) {
        Customer allCustomers = applicationService.getCustomerById(id);
        logger.debug("Get customers executed");
        return new ResponseEntity<>(allCustomers, HttpStatus.OK);
    }


    //TODO implement PUT, DELETE, UPDATE methods
}
