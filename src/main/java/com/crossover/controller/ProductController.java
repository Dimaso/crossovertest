package com.crossover.controller;

import com.crossover.domain.Product;
import com.crossover.logic.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Dmitriy on 08.11.2015.
 * Controller for products.
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {

    private final static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    @Qualifier("applicationServiceImpl")
    private ApplicationService applicationService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<Product>> getProducts() {
        Iterable<Product> allProducts = applicationService.getAllProducts();
        logger.debug("Get products was executed");
        return new ResponseEntity<>(allProducts, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Product> getProduct(@PathVariable int id) {
        Product product = applicationService.getProductById(id);
        logger.debug("Get product was executed");
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    //TODO PUT and DELETE methods
}
