package com.crossover.controller;

import com.crossover.domain.OrderLine;
import com.crossover.logic.ApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Dmitriy on 08.11.2015.
 * Controller for Orderline
 */
@Controller
@RequestMapping(value = "/orderLine")
public class OrderLineController {

    private static final Logger logger = LoggerFactory.getLogger(OrderLineController.class);

    @Autowired
    @Qualifier("applicationServiceImpl")
    private ApplicationService applicationService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Iterable<OrderLine>> getCustomers() {
        Iterable<OrderLine> allCustomers = applicationService.getAllOrderLines();
        logger.debug("Get customers executed");
        return new ResponseEntity<>(allCustomers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<OrderLine> getCustomer(@PathVariable int id) {
        OrderLine allCustomers = applicationService.getOrderLineById(id);
        logger.debug("Get customers executed");
        return new ResponseEntity<>(allCustomers, HttpStatus.OK);
    }

    //TODO implement GET, PUT, DELETE, UPDATE methods
}
