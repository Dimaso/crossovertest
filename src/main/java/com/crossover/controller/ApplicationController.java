package com.crossover.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Dmitriy on 25.10.2015.
 * Application general controller
 */
@Controller
public class ApplicationController {

    private static final String VIEW_INDEX = "index";
    private final static Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome() {
        logger.debug("Welcome page displayed");
        return VIEW_INDEX;
    }
}
