package com.crossover.repository;

import com.crossover.domain.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dmitriy on 25.10.2015.
 * CRUD fot Orders
 */
public interface OrderRepository extends CrudRepository<Order, Integer> {
}
