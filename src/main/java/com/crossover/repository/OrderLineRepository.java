package com.crossover.repository;

import com.crossover.domain.OrderLine;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dmitriy on 25.10.2015.
 * CRUD fot OrderLines
 */
public interface OrderLineRepository extends CrudRepository<OrderLine, Integer> {
}
