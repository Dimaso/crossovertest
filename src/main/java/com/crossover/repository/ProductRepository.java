package com.crossover.repository;

import com.crossover.domain.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dmitriy on 25.10.2015.
 * CRUD fot Products
 */
public interface ProductRepository extends CrudRepository<Product, Integer > {
}
