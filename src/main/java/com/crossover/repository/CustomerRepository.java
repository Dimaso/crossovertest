package com.crossover.repository;

import com.crossover.domain.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dmitriy on 25.10.2015.
 * CRUD fot Customers
 */

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
}
