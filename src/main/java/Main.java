import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Dmitriy on 25.10.2015.
 * Main class of Application
 */
public class Main {

    public static void main(String... args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println("HELLO");
    }
}
