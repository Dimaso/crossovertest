#Back-end for Sales Order Application

DESCRIPTION
==================================================
Back-end for Sales Order Application is an application 
that allows to manage: 

1. Products
2. Customers
3. Sales Orders
4. Order lines

Basically this application allows you to provide create-read-update-delete operations on Entities.

Back-end also contains business logic for new order creation.
If Sales Order is valid, application backend  will reduce Product Quantities and increase
current credit for Customer.

#NOTE!
Currently Backend application is not connected to UI
provided, however it can accept GET, POST requests after
deploying via curl or Postman.  
I did not have time to implement all application.
(See APPLICATION ACCESS part)

TECHNOLOGIES
======================================================

Backend application uses technologies and products
listed below:

1. MySQL 5.7.9.0 as a database
2. Maven 3.2.5 as a build and dependency management tool
3. Hibernate hibernate-jpa-2.1-apo 1.0.0.Final as ORM
4. Spring data framework on top of Hibernate 1.9.0.RELEASE
5. Spring as IOC 4.1.7.RELEASE
6. JUnit 4.12 vesion as a testing framework
7. Spring Test 4.1.7.RELEASE as IOC for JUnit
8. Spring web as MVC framework 4.1.7.RELEASE version
9. Jackson Databind for JSO marshaling and unmarshaling
10. Tomcat 8.0.28 as middleware web container
11. JDK 8


BUILD
========================================================
Before building project it is needed to make database to
made tests run successfully during build.

1. Install or use existent MySQL server.
Default app connection preferences:

> jdbc.driverClassName=com.mysql.jdbc.Driver

> jdbc.url=jdbc:mysql://localhost:3306/sales_db

> jdbc.username=root

> jdbc.password=secret

2. Or provide database with same configuration on local env
or modify connection preferences in jdbc.properties file

3. Create sales_db database
use create_sales_database.sql file

4. Create database schema
use create_sales_schema.sql file

5. Insert test data
use insert_sales_data.sql file

6. Run "mnv clean install" maven task
to build project and also run all tests

7. In target folder yuo will find crossoverWebApp.war
that can be placed in tomcat (see deployment folder)

DEPLOYMENT
=========================================================
Please refer to [Deployment How-TO document](./DEPLOYMENT_README.md)

APPLICATION ACCESS
=========================================================
After deployment it is possible to make request below to 
retrieve/upload data to the Sales backend:
if you are running tomcat on localhost

1. GET localhost:8080/crossoverWebApp/order - get all orders
2. GET localhost:8080/crossoverWebApp/customer - get all customers
3. GET localhost:8080/crossoverWebApp/product - get all products
4. POST localhost:8080/crossoverWebApp/order - place new order
Request boby should contain JSON with order, for example:
`"{"orderId":null,"orderLines":[{"orderLineId":null,"product":{"productId":4,"description":"falcon","price":0.0,"quantity":0},"quantity":2}],"customer":{"customerId":2,"name":null,"creditLimit":0.0,"currentCredit":0.0,"address":null,"phone1":null,"phone2":null},"date":1445937307578,"totalPrice":0.0}"`
After POST request Server will:
    * Validate request
    * Check that is enough products in database to complete this order
    * Check that is enough credit limit left fro  customer
    * Place order and update customer, customer order, orderline, product tables with new calculated values.
5. GET localhost:8080/crossoverWebApp/product/{productId} - get specific product with id productId
6. DELETE localhost:8080/crossoverWebApp/order{orderId} - delete specific order with id order id. Also related orderLines will be deleted
7. PUT localhost:8080/crossoverWebApp/order/{id}  - Update given order with given orderId.
PUT request should also contain Request body, same template as for POST request.
Same business logic as for creating new order will be triggered.
